#include "Mandato.h"

Mandato::Mandato(char start, char fin) {
    //constructor
    startChar = start;
    endChar = fin;
    msg = ""; //mensaje vacio
    msgListo = false; //mensaje no est� listo
}
String Mandato::readMandato(void) {
    //leemos el mandato, lo vaciamos, y bajamos banderilla
    String res;

    //leemos (no comprobamos):
    res = msg;
    //vaciamos:
    msg = "";
    //bajamos banderilla:
    msgListo = false;

    return res;
}

void Mandato::set_msgListo(bool estado) {
    //ponemos la banderilla a true o false
    msgListo = estado;
    //si false tmbn vaciamos el string:
    if (estado == false) {
        msg = "";
    }
}