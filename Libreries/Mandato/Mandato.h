#pragma once
#ifndef Mandato_h
#define Mandato_h

#include "Arduino.h"

class Mandato
{
private:
    String msg; //mensaje de entrada
    char startChar; //char de inicio de mandato
    char endChar; //char de fin de mandato
    bool msgListo; //banderilla para saber si est� listo un mensaje
public:
    Mandato(char start, char fin);
    void writeMandatoChar(char in) { msg += in; }
    String readMandato(void);
    bool disponible(void) { return msgListo; }
    void set_msgListo(bool estado);
    char get_startChar(void) { return startChar; }
    char get_endChar(void) { return endChar; }
};

#endif // !Mandato_h
