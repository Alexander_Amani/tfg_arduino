#include "AMP.h"

AMP::AMP(uint16_t resol,float voltRef, uint8_t pin){
      //pasamos los parametros necesarios:
      _resolucion=resol;
      _vref=voltRef;
      _pin=pin;
      //offset y scale a 0:
      offset=0;
      scale=1;
}

float AMP::analogReadMean(uint16_t n){
    /*En esta función realizamos varias lecturas seguidas
       * hacemos la media de todas ellas
      */
      float buff=0;
      
      for(uint16_t i=0; i<n; i++){
        buff+=analogRead(_pin); //llenamos el buffer
      }
      buff=buff/n; //dividimos entre el número de muestras
    
      return buff;    
}

float AMP::analogShift(float vin){
      /*En esta función se obtiene la tensión real a partir de
       * una tensión desplazada por un divisor de tension con dos
       * resistencias iguales.
      */
    
      float res;
    
      res=_vref*(2*vin/_resolucion-1);
      
      return res;
}

float AMP::lecturaINA(uint16_t n){
      /*Aqui se realiza toda la lectura del INA por simplicidad
      */
      float vin,vout;
      vin=analogReadMean(n); //leemos n veces
      vout=analogShift(vin); //obtenemos la señal antes de desplazarla
      vout=scale*(vout-offset); // quitamos el offset y cambiamos el factor de escala
    
      return vout;
      
    }