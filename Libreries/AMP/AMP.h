#pragma once
#ifndef AMP_h
#define AMP_h

#include "Arduino.h"

class AMP{
    private:
        float offset; //valor de offset de la lectura del amplificador
        float scale; //factor de escala para pasar de voltios a gramos

        uint16_t _resolucion; //resolución del ADC
        float _vref; //tensión de referencia del ADC (tipicamente en arduino 5V)
        uint8_t _pin; //pin analógico de entrada
    public:
        AMP(uint16_t resol,float voltRef, uint8_t pin);
        void setOffset(float off){offset=off;}
        void setScale(float s){scale=s;}
        float getOffset(){return offset;}
        float getScale(){return scale;}
        float analogReadMean(uint16_t n); //devuelve la media de n lecturas
        float analogShift(float vin); //obtiene la tensión antes de ser desplazada con divisor de tensión
        float lecturaINA(uint16_t n); //realiza la lectura completa con las dos funciones anteriores
        void reset(void) { offset = 0; scale = 1; } //reseteamos el valor del offset y del factor de escala
};

#endif