#include <HX711.h>
#include <Mandato.h>
#include <AMP.h>

//defines:
//HX711 defines:
#define DTpin (uint8_t)12
#define CLKpin (uint8_t)13
//INA defines:
#define RESOLUCION (uint16_t)1024 //resolución del ADC de arduino UNO
#define VREF (float)4.96 //tensión de referencia
#define INApin (uint8_t) 19 //pin 19 = A5

//Variable globales:
Mandato msg('#','@'); //clase para recibir mandatos por Serial
String mensaje=""; //string donde se almacena el mandato
String msgConect="CONECTADO"; //mensaje de respuesta para confirmar conexión

HX711 myHX; //clase del HX711
AMP myAMP(RESOLUCION, VREF, INApin); //clase del amplificador

typedef enum{REPOSO, HX, INA}estados; //estados principales del sistema
estados estado=REPOSO; //iniciamos en reposo
estados sigEstado=REPOSO;

typedef enum{
  REPOSO_CHAR=(int)'r', //ir a estado de reposo
  HX_CHAR=(int)'h', //ir al estado de toma de datos del HX711
  HX_TARA=(int)'t', //realizamos la tara (igual quitamos esto)
  HX_SCALE=(int)'s', //realizamos el calibrado automatico del factor de escala (igual se quita esto)
  INA_CHAR=(int)'i', //ir al estado del amplificador

  OFFSET_CHAR=(int)'o', //ajuste manual del offset (slider)
  FACTOR_CHAR=(int)'f', //ajusto manual del factor de escala (slider)
  RESET_CHAR=(int)'z', //se pone a 0 el offset y a 1 el factor de escala
  GAIN_CHAR=(int)'g', //cambiamos la ganancia entre 128 o 64
  CONECT_CHAR=(int)'c' //mensaje para comprobar si estamos conectados al micro correctamente
}charMandato; //enum con los caracteres que definen el estado

//prototipo de las funciones:
void msgFSM(char inChar);
estados siguienteEstado(String mensaje, estados estado);
void salidas(estados estado);

void setup() {
  //inicializacimos la comunicación serie:
  Serial.begin(9600); //de momento solo 9600
  while(!Serial); //por si acaso

  //Inicializamos el HX711:
  myHX.begin(DTpin,CLKpin);
  

}

void loop() {

  if(msg.disponible()){ //si hay un mensaje disponible:
    //leemos:
    mensaje=msg.readMandato();
    //calculamos el siguiente estado:
    estado=siguienteEstado(mensaje,estado);
  }
  salidas(estado);

}

void msgFSM(char inChar){
  /*En esta función se realiza la lógica de la máquina de estados
   * de la recepción de la consigna
   * luego se ejecuta en serialEvent()
  */
  static uint8_t estado=0; // 0: esperando start 1: esperando end
  
  //máquina de estados:
    switch(estado){
      case 0: //esperando start
        if(inChar==msg.get_startChar()){
          //reiniciamos el mensaje y cambiamos de estado:
          msg.set_msgListo(false);
          estado=1;
          }
        break;
        
      case 1: //esperando fin:
        if(inChar==msg.get_endChar()){
          msg.set_msgListo(true);
          estado=0;
          }
        else if(inChar==msg.get_startChar()){
          //por seguridad reiniciamos el mensaje:
          msg.set_msgListo(false);
          }
        else{
          //cualquier caracter lo metemos en el mensaje:
          msg.writeMandatoChar(inChar);
          }
        break;
        
      default:
        //no deberia pero si pasa:
        estado=0;
        break;
    }
}

estados siguienteEstado(String mensaje, estados estado){
  /*En esta función se calcula el siguiente estado en función
   * del mensaje recibido y del estado actual.
   * de momento solo 3 estados: REPOSO, HX711, INA
  */
  float scalePeso; //aqui se almacena el valor para el calibrado del factor
  
  switch(estado){
    
    case REPOSO:
      if(mensaje[0]==(char)HX_CHAR){return HX;}
      else if(mensaje[0]==(char)INA_CHAR){return INA;}
      else if(mensaje[0]==(char)CONECT_CHAR){
        Serial.println(msgConect);
        return REPOSO;
      }
      else{return REPOSO;}
      break;
      
    case HX:
      if(mensaje[0]==(char)REPOSO_CHAR){return REPOSO;}
      else if(mensaje[0]==(char)INA_CHAR){return INA;}

      else if(mensaje[0]==(char)HX_TARA){
          Serial.println("ESTAMOS TARANDO...");
          myHX.tare(30); //aprox 3s
      }
      else if(mensaje[0]==(char)HX_SCALE){
        Serial.println("CALIBRANDO FACTOR DE ESCALA...");
        scalePeso=mensaje.substring(1).toFloat();
        myHX.calibrate_scale(scalePeso,30);
      }
      else if(mensaje[0]==(char)OFFSET_CHAR){
        myHX.set_offset(mensaje.substring(1).toFloat());
        return HX;
      }
      else if(mensaje[0]==(char)FACTOR_CHAR){
        myHX.set_scale(mensaje.substring(1).toFloat()); //OJO ES EL INVERSO DE EL FACTOR DE ESCALA
        return HX;
      }
      else if(mensaje[0]==(char)RESET_CHAR){
        myHX.set_offset(0);
        myHX.set_scale(1);
        //no usamos el reset de la librería porque no queremos resetear la ganancia.
        return HX;
      }
      else if(mensaje[0]==(char)GAIN_CHAR){
        //primero hacemos reset:
        myHX.reset();
        //cambiamos la ganancia:
        myHX.set_gain((uint8_t)mensaje.substring(1).toInt());
        return HX;
      }
      else if(mensaje[0]==(char)CONECT_CHAR){
        Serial.println(msgConect);
        return HX;
      }
      else{return HX;}
      break;
      
    case INA:
      if(mensaje[0]==(char)HX_CHAR){return HX;}
      else if(mensaje[0]==(char)REPOSO_CHAR){return REPOSO;}
      else if(mensaje[0]==(char)OFFSET_CHAR){
        myAMP.setOffset(mensaje.substring(1).toFloat());
        return INA;
      }
      else if(mensaje[0]==(char)FACTOR_CHAR){
        myAMP.setScale(mensaje.substring(1).toFloat());
        return INA;
      }
      else if(mensaje[0]==(char)RESET_CHAR){
        myAMP.reset();
        return INA;
      }
      else if(mensaje[0]==(char)CONECT_CHAR){
        Serial.println(msgConect);
        return INA;
      }
      else{return INA;}
      break;
      
    default:
      //raro, pero si pasa que vuelva a reposo:
      return REPOSO;
      break;
  }
  //si por algun casual llegasemos aquí:
  return estado;
  
}

void salidas(estados estado){
  /*En esta función se gestionan las salidas en función del estado
   * de momento como prueba solo mandamos un mensajito:
  */
  //variable HX711:
  float hxBits;
  float peso;
  String hxString="";

  //variables amplificador:
  String INAstring="";
  
  switch(estado){
    case REPOSO:
      Serial.println("REPOSANDO...");
      delay(1000);
      break;
      
    case HX:
      hxBits=myHX.read_average(10);
      peso=(hxBits-myHX.get_offset())/myHX.get_scale();
      //mandamos H bits; peso; offset(bits); escala(INVERSO)
      hxString='H'+String(hxBits,0)+';'+String(peso,1)+';'+String(myHX.get_offset())+';'+String(myHX.get_scale());
      Serial.println(hxString);
      break;
      
    case INA:
      INAstring='I'+String(myAMP.lecturaINA(1024),3)+';'+String(myAMP.getOffset(),3)+';'+String(myAMP.getScale(),2);
      Serial.println(INAstring);
      delay(250);
      break;
      
    default:
    //no deberia pero si pasa:
      break;
  }
}







void serialEvent(){
/*Esta función se llama al final del loop SI se ha recibido algo por RX:
*/
  char inChar; //caracter que vamos a añadir
  
  //mientras haya caracteres:
  while(Serial.available()){
    //leemos el dato:
    inChar=(char)Serial.read();
    //máquina de estados:
    msgFSM(inChar);
  }  
}
